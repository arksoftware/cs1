IF (OBJECT_ID(N'cs1.AccountEntry') IS NULL)
	CREATE TABLE [cs1].[AccountEntry] (
        [EntryDate] DATETIME NOT NULL,
		[EntryId] BIGINT IDENTITY(1,1),
		[SourceAccountId] BIGINT NOT NULL,
        [EntryTypeId] INT NOT NULL,
        [TargetAccountId] BIGINT NULL,
        [Amount] MONEY NOT NULL);
GO

IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE [object_id] = OBJECT_ID(N'cs1.AccountEntry') AND [name] = N'ix_AccountEntry_EntryDate_EntryId')
	CREATE UNIQUE CLUSTERED INDEX [ix_AccountEntry_EntryDate_EntryId]
		ON [cs1].[AccountEntry] ([EntryDate], [EntryId]);
GO
