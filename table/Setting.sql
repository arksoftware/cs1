IF (OBJECT_ID(N'cs1.Setting') IS NULL)
	CREATE TABLE [cs1].[Setting] (
		[SettingName] VARCHAR(32) NOT NULL,
		[SettingValue] NVARCHAR(4000) NULL);
GO

IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE [object_id] = OBJECT_ID(N'cs1.Setting') AND [name] = N'ix_Setting_SettingName')
	CREATE UNIQUE CLUSTERED INDEX [ix_Setting_SettingName]
		ON [cs1].[Setting] ([SettingName]);
GO

IF NOT EXISTS (SELECT 1 FROM [cs1].[Setting] WHERE [SettingName] = 'BankName')
    INSERT INTO [cs1].[Setting] ([SettingName], [SettingValue]) SELECT 'BankName', N'Medici';
GO
