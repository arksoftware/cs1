IF (OBJECT_ID(N'cs1.EntryType') IS NULL)
	CREATE TABLE [cs1].[EntryType] (
		[TypeId] INT NOT NULL,
		[TypeName] VARCHAR(100) NOT NULL);
GO

IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE [object_id] = OBJECT_ID(N'cs1.EntryType') AND [name] = N'ix_EntryType_TypeId')
	CREATE UNIQUE CLUSTERED INDEX [ix_EntryType_TypeId]
		ON [cs1].[EntryType] ([TypeId]);
GO

IF NOT EXISTS (SELECT 1 FROM [cs1].[EntryType] WHERE [TypeId] = 1)
    INSERT INTO [cs1].[EntryType] ([TypeId], [TypeName]) SELECT 1, 'Deposit';
IF NOT EXISTS (SELECT 1 FROM [cs1].[EntryType] WHERE [TypeId] = 2)
    INSERT INTO [cs1].[EntryType] ([TypeId], [TypeName]) SELECT 2, 'Withdrawal';
IF NOT EXISTS (SELECT 1 FROM [cs1].[EntryType] WHERE [TypeId] = 3)
    INSERT INTO [cs1].[EntryType] ([TypeId], [TypeName]) SELECT 3, 'Transfer';
GO
