IF (OBJECT_ID(N'cs1.Account') IS NULL)
	CREATE TABLE [cs1].[Account] (
		[AccountId] BIGINT IDENTITY(1,1),
		[OwnerId] BIGINT NOT NULL,
        [AccountTypeId] INT NOT NULL);
GO

IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE [object_id] = OBJECT_ID(N'cs1.Account') AND [name] = N'ix_Account_AccountId')
	CREATE UNIQUE CLUSTERED INDEX [ix_Account_AccountId]
		ON [cs1].[Account] ([AccountId]);
GO
