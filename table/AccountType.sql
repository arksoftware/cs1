IF (OBJECT_ID(N'cs1.AccountType') IS NULL)
	CREATE TABLE [cs1].[AccountType] (
		[TypeId] INT NOT NULL,
		[TypeName] VARCHAR(100) NOT NULL,
        [WithdrawalLimit] MONEY NULL);
GO

IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE [object_id] = OBJECT_ID(N'cs1.AccountType') AND [name] = N'ix_AccountType_TypeId')
	CREATE UNIQUE CLUSTERED INDEX [ix_AccountType_TypeId]
		ON [cs1].[AccountType] ([TypeId]);
GO

IF NOT EXISTS (SELECT 1 FROM [cs1].[AccountType] WHERE [TypeId] = 1)
    INSERT INTO [cs1].[AccountType] ([TypeId], [TypeName], [WithdrawalLimit]) SELECT 1, 'Checking', NULL;
IF NOT EXISTS (SELECT 1 FROM [cs1].[AccountType] WHERE [TypeId] = 2)
    INSERT INTO [cs1].[AccountType] ([TypeId], [TypeName], [WithdrawalLimit]) SELECT 2, 'Corporate Investment', NULL;
IF NOT EXISTS (SELECT 1 FROM [cs1].[AccountType] WHERE [TypeId] = 3)
    INSERT INTO [cs1].[AccountType] ([TypeId], [TypeName], [WithdrawalLimit]) SELECT 3, 'Individual Investment', 1000;
GO
