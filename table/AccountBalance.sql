IF (OBJECT_ID(N'cs1.AccountBalance') IS NULL)
	CREATE TABLE [cs1].[AccountBalance] (
		[AccountId] BIGINT NOT NULL,
        [EntryDate] DATETIME NOT NULL,
		[EntryId] BIGINT NOT NULL,
        [Balance] MONEY NOT NULL);
GO

IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE [object_id] = OBJECT_ID(N'cs1.AccountBalance') AND [name] = N'ix_AccountBalance_AccountId')
	CREATE UNIQUE CLUSTERED INDEX [ix_AccountBalance_AccountId]
		ON [cs1].[AccountBalance] ([AccountId]);
GO
