SET NOCOUNT ON;
DECLARE
    @balance MONEY,
    @count INT,
    @entryDate DATETIME,
    @entryId BIGINT,
    @errorExpected INT,
    @errorReturned INT,
    @i INT = 1,
    @msg NVARCHAR(4000),
    @rc INT,
    @sourceBalance MONEY,
    @sourceId BIGINT,
    @stmt NVARCHAR(4000),
    @targetBalance MONEY,
    @targetId BIGINT;
IF (OBJECT_ID(N'tempdb..#t') IS NOT NULL) DROP TABLE #t;
CREATE TABLE #t ([i] INT IDENTITY(1,1) PRIMARY KEY, [stmt] NVARCHAR(MAX), [error] INT, [sourceBalance] MONEY, [targetBalance] MONEY);
INSERT INTO #t ([stmt], [error], [sourceBalance], [targetBalance])
    SELECT N'EXEC [cs1].[AccountEntry_Insert] @SourceAccountId = NULL, @TargetAccountId = NULL, @EntryTypeId = 1, @Amount = 1, @EntryDate = @entryDate OUT, @EntryId = @entryId OUT;', 50201, NULL, NULL
    UNION ALL
    SELECT N'EXEC [cs1].[AccountEntry_Insert] @SourceAccountId = 1, @TargetAccountId = NULL, @EntryTypeId = 1, @Amount = NULL, @EntryDate = @entryDate OUT, @EntryId = @entryId OUT;', 50202, NULL, NULL
    UNION ALL
    SELECT N'EXEC [cs1].[AccountEntry_Insert] @SourceAccountId = 1, @TargetAccountId = NULL, @EntryTypeId = 1, @Amount = -0.0001, @EntryDate = @entryDate OUT, @EntryId = @entryId OUT;', 50203, NULL, NULL
    UNION ALL
    SELECT N'EXEC [cs1].[AccountEntry_Insert] @SourceAccountId = 1, @TargetAccountId = NULL, @EntryTypeId = 1, @Amount = 0, @EntryDate = @entryDate OUT, @EntryId = @entryId OUT;', 50203, NULL, NULL
    UNION ALL
    SELECT N'EXEC [cs1].[AccountEntry_Insert] @SourceAccountId = 1, @TargetAccountId = 2, @EntryTypeId = 1, @Amount = 1, @EntryDate = @entryDate OUT, @EntryId = @entryId OUT;', 50204, NULL, NULL
    UNION ALL
    SELECT N'EXEC [cs1].[AccountEntry_Insert] @SourceAccountId = 1, @TargetAccountId = 2, @EntryTypeId = 2, @Amount = 1, @EntryDate = @entryDate OUT, @EntryId = @entryId OUT;', 50204, NULL, NULL
    UNION ALL
    SELECT N'EXEC [cs1].[AccountEntry_Insert] @SourceAccountId = 1, @TargetAccountId = NULL, @EntryTypeId = 1, @Amount = 1, @EntryDate = @entryDate OUT, @EntryId = @entryId OUT;', 50205, NULL, NULL
    UNION ALL
    SELECT N'EXEC [cs1].[AccountEntry_Insert] @SourceAccountId = 1, @TargetAccountId = NULL, @EntryTypeId = 2, @Amount = 1, @EntryDate = @entryDate OUT, @EntryId = @entryId OUT;', 50205, NULL, NULL
    UNION ALL
    SELECT N'DECLARE @sourceId BIGINT; EXEC [cs1].[Account_Insert] @AccountId = @sourceId OUT, @OwnerId = 1, @AccountTypeId = 3;
EXEC [cs1].[AccountEntry_Insert] @SourceAccountId = @sourceId, @TargetAccountId = NULL, @EntryTypeId = 2, @Amount = 1000.0001, @EntryDate = @entryDate OUT, @EntryId = @entryId OUT;', 50206, NULL, NULL
    UNION ALL
    SELECT N'DECLARE @sourceId BIGINT; EXEC [cs1].[Account_Insert] @AccountId = @sourceId OUT, @OwnerId = 1, @AccountTypeId = 1;
EXEC [cs1].[AccountEntry_Insert] @SourceAccountId = @sourceId, @TargetAccountId = NULL, @EntryTypeId = 2, @Amount = 0.0001, @EntryDate = @entryDate OUT, @EntryId = @entryId OUT;', 50207, NULL, NULL
    UNION ALL
    SELECT N'DECLARE @sourceId BIGINT; EXEC [cs1].[Account_Insert] @AccountId = @sourceId OUT, @OwnerId = 1, @AccountTypeId = 2;
EXEC [cs1].[AccountEntry_Insert] @SourceAccountId = @sourceId, @TargetAccountId = NULL, @EntryTypeId = 2, @Amount = 0.0001, @EntryDate = @entryDate OUT, @EntryId = @entryId OUT;', 50207, NULL, NULL
    UNION ALL
    SELECT N'DECLARE @sourceId BIGINT; EXEC [cs1].[Account_Insert] @AccountId = @sourceId OUT, @OwnerId = 1, @AccountTypeId = 3;
EXEC [cs1].[AccountEntry_Insert] @SourceAccountId = @sourceId, @TargetAccountId = NULL, @EntryTypeId = 2, @Amount = 0.0001, @EntryDate = @entryDate OUT, @EntryId = @entryId OUT;', 50207, NULL, NULL
    UNION ALL
    SELECT N'DECLARE @sourceId BIGINT; EXEC [cs1].[Account_Insert] @AccountId = @sourceId OUT, @OwnerId = 1, @AccountTypeId = 2;
EXEC [cs1].[AccountEntry_Insert] @SourceAccountId = @sourceId, @TargetAccountId = NULL, @EntryTypeId = 1, @Amount = 1, @EntryDate = @entryDate OUT, @EntryId = @entryId OUT;
EXEC [cs1].[AccountEntry_Insert] @SourceAccountId = @sourceId, @TargetAccountId = NULL, @EntryTypeId = 3, @Amount = 0.0001, @EntryDate = @entryDate OUT, @EntryId = @entryId OUT;', 50208, NULL, NULL
    UNION ALL
    SELECT N'DECLARE @sourceId BIGINT, @targetId BIGINT; EXEC [cs1].[Account_Insert] @AccountId = @sourceId OUT, @OwnerId = 1, @AccountTypeId = 2;
EXEC [cs1].[AccountEntry_Insert] @SourceAccountId = @sourceId, @TargetAccountId = NULL, @EntryTypeId = 1, @Amount = 1, @EntryDate = @entryDate OUT, @EntryId = @entryId OUT;
SELECT @targetId = @sourceId * -1;
EXEC [cs1].[AccountEntry_Insert] @SourceAccountId = @sourceId, @TargetAccountId = @targetId, @EntryTypeId = 3, @Amount = 0.0001, @EntryDate = @entryDate OUT, @EntryId = @entryId OUT;', 50209, NULL, NULL
    UNION ALL
    SELECT N'EXEC [cs1].[AccountEntry_Insert] @SourceAccountId = 1, @TargetAccountId = NULL, @EntryTypeId = 0, @Amount = 1, @EntryDate = @entryDate OUT, @EntryId = @entryId OUT;', 50210, NULL, NULL
    UNION ALL
    SELECT N'EXEC [cs1].[AccountEntry_Insert] @SourceAccountId = 1, @TargetAccountId = NULL, @EntryTypeId = 4, @Amount = 1, @EntryDate = @entryDate OUT, @EntryId = @entryId OUT;', 50210, NULL, NULL
    UNION ALL
    SELECT N'EXEC [cs1].[AccountEntry_Insert] @SourceAccountId = 1, @TargetAccountId = 1, @EntryTypeId = 3, @Amount = 1, @EntryDate = @entryDate OUT, @EntryId = @entryId OUT;', 50211, NULL, NULL
    UNION ALL
    SELECT N'DECLARE @sourceId BIGINT; EXEC [cs1].[Account_Insert] @AccountId = @sourceId OUT, @OwnerId = 1, @AccountTypeId = 1;
EXEC [cs1].[AccountEntry_Insert] @SourceAccountId = @sourceId, @TargetAccountId = NULL, @EntryTypeId = 1, @Amount = 1, @EntryDate = @entryDate OUT, @EntryId = @entryId OUT;', 0, 1, NULL
    UNION ALL
    SELECT N'DECLARE @sourceId BIGINT; EXEC [cs1].[Account_Insert] @AccountId = @sourceId OUT, @OwnerId = 1, @AccountTypeId = 1;
EXEC [cs1].[AccountEntry_Insert] @SourceAccountId = @sourceId, @TargetAccountId = NULL, @EntryTypeId = 1, @Amount = 3, @EntryDate = @entryDate OUT, @EntryId = @entryId OUT;
EXEC [cs1].[AccountEntry_Insert] @SourceAccountId = @sourceId, @TargetAccountId = NULL, @EntryTypeId = 2, @Amount = 1, @EntryDate = @entryDate OUT, @EntryId = @entryId OUT;', 0, 2, NULL
    UNION ALL
    SELECT N'DECLARE @sourceId BIGINT, @targetId BIGINT;
EXEC [cs1].[Account_Insert] @AccountId = @sourceId OUT, @OwnerId = 1, @AccountTypeId = 1;
EXEC [cs1].[Account_Insert] @AccountId = @targetId OUT, @OwnerId = 1, @AccountTypeId = 1;
EXEC [cs1].[AccountEntry_Insert] @SourceAccountId = @sourceId, @TargetAccountId = NULL, @EntryTypeId = 1, @Amount = 3.0001, @EntryDate = @entryDate OUT, @EntryId = @entryId OUT;
EXEC [cs1].[AccountEntry_Insert] @SourceAccountId = @sourceId, @TargetAccountId = @targetId, @EntryTypeId = 3, @Amount = 0.9999, @EntryDate = @entryDate OUT, @EntryId = @entryId OUT;', 0, 2.0002, 0.9999;
SELECT @rc = @@ROWCOUNT;
WHILE (@i <= @rc)
BEGIN
    SELECT @stmt = [stmt], @errorExpected = [error], @sourceBalance = [sourceBalance], @targetBalance = [targetBalance] FROM #t WHERE [i] = @i;
    BEGIN TRANSACTION;
    BEGIN TRY
        EXEC sp_executesql
            @stmt = @stmt,
            @params = N'@entryDate DATETIME OUT, @entryId BIGINT OUT',
            @entryDate = @entryDate OUT,
            @entryId = @entryId OUT;
        SELECT @errorReturned = 0;
    END TRY
    BEGIN CATCH
        SELECT @errorReturned = @@ERROR;
    END CATCH;
    IF (@errorExpected != @errorReturned)
    BEGIN
        PRINT @stmt;
        SELECT @msg = FORMATMESSAGE(N'Expected %i, returned %i', @errorExpected, @errorReturned);
        PRINT @msg;
    END;
    ELSE IF (@errorExpected = 0)
    BEGIN
        IF (@entryDate IS NULL)
        BEGIN
            PRINT @stmt;
            SELECT @msg = N'Expected @entryDate, returned NULL';
            PRINT @msg;
        END;
        ELSE IF (@entryId IS NULL)
        BEGIN
            PRINT @stmt;
            SELECT @msg = N'Expected @entryId, returned NULL';
            PRINT @msg;
        END;
        ELSE
        BEGIN
            SELECT @count = COUNT(*) FROM [cs1].[AccountEntry] WHERE [EntryDate] = @entryDate AND [EntryId] = @entryId;
            IF (@count != 1)
            BEGIN
                PRINT @stmt;
                SELECT @msg = FORMATMESSAGE(N'Expected 1 row in cs1.AccountEntry, returned %i', @count);
                PRINT @msg;
            END;
            ELSE
            BEGIN
                SELECT @sourceId = [SourceAccountId], @targetId = [TargetAccountId]
                    FROM [cs1].[AccountEntry]
                    WHERE [EntryDate] = @entryDate
                        AND [EntryId] = @entryId;
                IF (@sourceBalance IS NOT NULL)
                BEGIN
                    SELECT @balance = b.[Balance]
                        FROM [cs1].[AccountEntry] AS e
                            INNER JOIN [cs1].[AccountBalance] AS b
                                ON e.[EntryDate] = @entryDate
                                AND e.[EntryId] = @entryId
                                AND e.[SourceAccountId] = b.[AccountId];
                    IF (@balance != @sourceBalance)
                    BEGIN
                        PRINT @stmt;
                        SELECT @msg = FORMATMESSAGE(N'Expected source balance of %s, returned %s', CAST(@sourceBalance AS NVARCHAR(4000)), CAST(@balance AS NVARCHAR(4000)));
                        PRINT @msg;
                    END;
                END;
                IF (@targetBalance IS NOT NULL)
                BEGIN
                    SELECT @balance = b.[Balance]
                        FROM [cs1].[AccountEntry] AS e
                            INNER JOIN [cs1].[AccountBalance] AS b
                                ON e.[EntryDate] = @entryDate
                                AND e.[EntryId] = @entryId
                                AND e.[TargetAccountId] = b.[AccountId];
                    IF (@balance != @targetBalance)
                    BEGIN
                        PRINT @stmt;
                        SELECT @msg = FORMATMESSAGE(N'Expected target balance of %s, returned %s', CAST(@targetBalance AS NVARCHAR(4000)), CAST(@balance AS NVARCHAR(4000)));
                        PRINT @msg;
                    END;
                END;
            END;
        END;
    END;
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION;
    SELECT @i = @i + 1;
END;
PRINT 'Tests Complete';
GO
