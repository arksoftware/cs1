SET NOCOUNT ON;
DECLARE
    @accountId BIGINT,
    @count INT,
    @errorExpected INT,
    @errorReturned INT,
    @i INT = 1,
    @msg NVARCHAR(4000),
    @rc INT,
    @stmt NVARCHAR(4000);
IF (OBJECT_ID(N'tempdb..#t') IS NOT NULL) DROP TABLE #t;
CREATE TABLE #t ([i] INT IDENTITY(1,1) PRIMARY KEY, [stmt] NVARCHAR(MAX), [error] INT);
INSERT INTO #t ([stmt], [error])
    SELECT N'EXEC [cs1].[Account_Insert] @AccountId = @accountId OUT, @OwnerId = NULL, @AccountTypeId = 1;', 50101
    UNION ALL
    SELECT N'EXEC [cs1].[Account_Insert] @AccountId = @accountId OUT, @OwnerId = 1, @AccountTypeId = NULL;', 50102
    UNION ALL
    SELECT N'EXEC [cs1].[Account_Insert] @AccountId = @accountId OUT, @OwnerId = 1, @AccountTypeId = 0;', 50103
    UNION ALL
    SELECT N'EXEC [cs1].[Account_Insert] @AccountId = @accountId OUT, @OwnerId = 1, @AccountTypeId = 1;', 0
    UNION ALL
    SELECT N'EXEC [cs1].[Account_Insert] @AccountId = @accountId OUT, @OwnerId = 1, @AccountTypeId = 2;', 0
    UNION ALL
    SELECT N'EXEC [cs1].[Account_Insert] @AccountId = @accountId OUT, @OwnerId = 1, @AccountTypeId = 3;', 0
    UNION ALL
    SELECT N'EXEC [cs1].[Account_Insert] @AccountId = @accountId OUT, @OwnerId = 1, @AccountTypeId = 4;', 50103;
SELECT @rc = @@ROWCOUNT;
WHILE (@i <= @rc)
BEGIN
    SELECT @stmt = [stmt], @errorExpected = [error] FROM #t WHERE [i] = @i;
    BEGIN TRANSACTION;
    BEGIN TRY
        EXEC sp_executesql @stmt = @stmt, @params = N'@accountId BIGINT OUT', @accountId = @accountId OUT;
        SELECT @errorReturned = 0;
    END TRY
    BEGIN CATCH
        SELECT @errorReturned = @@ERROR;
    END CATCH;
    IF (@errorExpected != @errorReturned)
    BEGIN
        PRINT @stmt;
        SELECT @msg = FORMATMESSAGE(N'Expected %i, returned %i', @errorExpected, @errorReturned);
        PRINT @msg;
    END;
    ELSE IF (@errorExpected = 0)
    BEGIN
        IF (@accountId IS NULL)
        BEGIN
            PRINT @stmt;
            SELECT @msg = N'Expected @accountId, returned NULL';
            PRINT @msg;
        END;
        ELSE IF (@accountId <= 0)
        BEGIN
            PRINT @stmt;
            SELECT @msg = FORMATMESSAGE(N'Expected @accountId, returned %i', @accountId);
            PRINT @msg;
        END;
        ELSE
        BEGIN
            SELECT @count = COUNT(*) FROM [cs1].[Account] WHERE [AccountId] = @accountId;
            IF (@count != 1)
            BEGIN
                PRINT @stmt;
                SELECT @msg = FORMATMESSAGE(N'Expected 1 row in cs1.Account, returned %i', @count);
                PRINT @msg;
            END;
            ELSE
            BEGIN
                SELECT @count = COUNT(*) FROM [cs1].[AccountBalance] WHERE [AccountId] = @accountId;
                IF (@count != 1)
                BEGIN
                    PRINT @stmt;
                    SELECT @msg = FORMATMESSAGE(N'Expected 1 row in cs1.AccountBalance, returned %i', @count);
                    PRINT @msg;
                END;
            END;
        END;
    END;
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION;
    SELECT @i = @i + 1;
END;
PRINT 'Tests Complete';
GO
