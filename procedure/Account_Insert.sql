IF (OBJECT_ID(N'cs1.Account_Insert') IS NULL)
	EXEC (N'CREATE PROCEDURE [cs1].[Account_Insert] AS RETURN 0;');
GO

ALTER PROCEDURE [cs1].[Account_Insert]
	@AccountId BIGINT OUTPUT,
    @OwnerId BIGINT,
	@AccountTypeId INT
AS
BEGIN
    SET XACT_ABORT ON;
    IF (@OwnerId IS NULL)
        THROW 50101, 'Owner ID Not Specified', 0;
    IF (@AccountTypeId IS NULL)
        THROW 50102, 'Account Type Not Specified', 0;
    IF NOT EXISTS (SELECT 1 FROM [cs1].[AccountType] WHERE [TypeId] = @AccountTypeId)
        THROW 50103, 'Invalid Account Type', 0;
    BEGIN TRANSACTION;
	INSERT INTO [cs1].[Account] (
            [OwnerID],
            [AccountTypeId])
		SELECT
			@OwnerId,
			@AccountTypeId;
    SELECT @AccountId = SCOPE_IDENTITY();
    INSERT INTO [cs1].[AccountBalance] (
            [AccountId],
            [EntryDate],
            [EntryId],
            [Balance])
        SELECT
            @AccountId,
            GETUTCDATE(),
            0,
            0;
    COMMIT TRANSACTION;
END;
GO
