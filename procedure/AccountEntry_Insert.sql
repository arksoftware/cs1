IF (OBJECT_ID(N'cs1.AccountEntry_Insert') IS NULL)
	EXEC (N'CREATE PROCEDURE [cs1].[AccountEntry_Insert] AS RETURN 0;');
GO

ALTER PROCEDURE [cs1].[AccountEntry_Insert]
	@SourceAccountId BIGINT,
    @TargetAccountId BIGINT,
    @EntryTypeId INT,
    @Amount MONEY,
    @EntryDate DATETIME OUT,
    @EntryId BIGINT OUT
AS
BEGIN
    SET XACT_ABORT ON;
    DECLARE @withdrawalLimit MONEY;
    IF (@SourceAccountId IS NULL)
        THROW 50201, 'Account Not Specified', 0;
    IF (@Amount IS NULL)
        THROW 50202, 'Amount Not Specified', 0;
    IF (@Amount <= 0)
        THROW 50203, 'Amount Invalid', 0;
    IF (@EntryTypeId = 1) /* Deposit */
    BEGIN
        IF (@TargetAccountId IS NOT NULL)
            THROW 50204, 'Target Account Specified', 0;
        BEGIN TRANSACTION;
        IF NOT EXISTS (SELECT 1 FROM [cs1].[Account] WHERE [AccountId] = @SourceAccountId)
            THROW 50205, 'Invalid Account ID', 0;
        SELECT @EntryDate = GETUTCDATE();
        INSERT INTO [cs1].[AccountEntry] (
                [EntryDate],
                [SourceAccountId],
                [EntryTypeId],
                [TargetAccountId],
                [Amount])
            SELECT
                @EntryDate,
                @SourceAccountId,
                1, /* Deposit */
                NULL,
                @Amount;
        SELECT @EntryId = SCOPE_IDENTITY();
        UPDATE [cs1].[AccountBalance]
            SET
                [EntryDate] = @EntryDate,
                [EntryId] = @EntryId,
                [Balance] = [Balance] + @Amount
            WHERE [AccountId] = @SourceAccountId;
        COMMIT TRANSACTION;
    END;
    ELSE IF (@EntryTypeId = 2) /* Withdrawal */
    BEGIN
        IF (@TargetAccountId IS NOT NULL)
            THROW 50204, 'Target Account Specified', 0;
        BEGIN TRANSACTION;
        IF NOT EXISTS (SELECT 1 FROM [cs1].[Account] WHERE [AccountId] = @SourceAccountId)
            THROW 50205, 'Invalid Account ID', 0;
        IF (@Amount > (SELECT t.[WithdrawalLimit]
                FROM [cs1].[Account] AS a
                    INNER JOIN [cs1].[AccountType] AS t
                        ON a.[AccountId] = @SourceAccountId
                        AND a.[AccountTypeId] = t.[TypeId]))
            THROW 50206, 'Withdrawal Limit Exceeded', 0;
        IF (@Amount > (SELECT b.[Balance]
                FROM [cs1].[AccountBalance] AS b
                WHERE b.[AccountId] = @SourceAccountId))
            THROW 50207, 'Account Overdraft', 0;
        SELECT @EntryDate = GETUTCDATE();
        INSERT INTO [cs1].[AccountEntry] (
                [EntryDate],
                [SourceAccountId],
                [EntryTypeId],
                [TargetAccountId],
                [Amount])
            SELECT
                @EntryDate,
                @SourceAccountId,
                2, /* Withdrawal */
                NULL,
                @Amount;
        SELECT @EntryId = SCOPE_IDENTITY();
        UPDATE [cs1].[AccountBalance]
            SET
                [EntryDate] = @EntryDate,
                [EntryId] = @EntryId,
                [Balance] = [Balance] - @Amount
            WHERE [AccountId] = @SourceAccountId;
        COMMIT TRANSACTION;
    END;
    ELSE IF (@EntryTypeId = 3) /* Transfer */
    BEGIN
        IF (@TargetAccountId IS NULL)
            THROW 50208, 'Target Account Not Specified', 0;
        BEGIN TRANSACTION;
        IF (@SourceAccountId = @TargetAccountId)
            THROW 50211, 'Source and Target Account IDs are Identical', 0;
        IF NOT EXISTS (SELECT 1 FROM [cs1].[Account] WHERE [AccountId] = @SourceAccountId)
            THROW 50205, 'Invalid Account ID', 0;
        IF NOT EXISTS (SELECT 1 FROM [cs1].[Account] WHERE [AccountId] = @TargetAccountId)
            THROW 50209, 'Invalid Target Account ID', 0;
        IF (@Amount > (SELECT b.[Balance]
                FROM [cs1].[AccountBalance] AS b
                WHERE b.[AccountId] = @SourceAccountId))
            THROW 50207, 'Account Overdraft', 0;
        SELECT @EntryDate = GETUTCDATE();
        INSERT INTO [cs1].[AccountEntry] (
                [EntryDate],
                [SourceAccountId],
                [EntryTypeId],
                [TargetAccountId],
                [Amount])
            SELECT
                @EntryDate,
                @SourceAccountId,
                3, /* Transfer */
                @TargetAccountId,
                @Amount;
        SELECT @EntryId = SCOPE_IDENTITY();
        UPDATE [cs1].[AccountBalance]
            SET
                [EntryDate] = @EntryDate,
                [EntryId] = @EntryId,
                [Balance] = [Balance] - @Amount
            WHERE [AccountId] = @SourceAccountId;
        UPDATE [cs1].[AccountBalance]
            SET
                [EntryDate] = @EntryDate,
                [EntryId] = @EntryId,
                [Balance] = [Balance] + @Amount
            WHERE [AccountId] = @TargetAccountId;
        COMMIT TRANSACTION;
    END;
    ELSE
        THROW 50210, 'Invalid Entry Type', 0;
END;
GO
