# cs1

## Details

This is a submission for a coding exercise, intended to *minimally* meet a set of requirements that were provided.

* This is expected to run on SQL Server 2016 and above.
* The interface consists of two procedures, cs1.Account_Insert and cs1.AccountEntry_Insert.
* Tests of both procedures are also supplied.
* To deploy, run:
    * schema.sql
    * The contents of the table folder
    * The contents of the procedure folder

## Original Request

### Description

One of our partners has requested a simple banking application to be developed. No user interface is
necessary for this application however unit tests are required to verify that all requirements for the
application have been met.

### Requirements

* A bank that has a name as well as a list of accounts
* The following types of accounts: Checking, Corporate Investment, Individual Investment
* All accounts must have an owner.
* All accounts must support deposits, withdrawals, and transfers to any other type of account
* Individual Investment accounts can withdraw up to $1,000 at a time
* It is not permissible to overdraft an account
