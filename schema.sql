IF NOT EXISTS (SELECT 1 FROM sys.database_principals WHERE [name] = N'cs1')
	CREATE USER [cs1] WITHOUT LOGIN;
GO

IF NOT EXISTS (SELECT 1 FROM sys.schemas WHERE [name] = N'cs1')
	EXEC (N'CREATE SCHEMA [cs1] AUTHORIZATION [cs1];');
GO
